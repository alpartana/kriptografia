#!/usr/bin/env python3 -tt
"""
File: crypto.py
---------------
Assignment 1: Cryptography
Course: CS 41
Name: <YOUR NAME>
SUNet: <SUNet ID>

Replace this with a description of the program.
"""
import utils

# Caesar Cipher

def encrypt_caesar(plaintext):
    """Encrypt plaintext using a Caesar cipher.
    """
    ciphertext = ""
    for letter in plaintext:
        if ord(letter.upper()) <= 90 and ord(letter.upper()) >= 65:
            cipherletter =  chr((ord(letter.upper()) - 62 ) % 26 + 65)
        else:
            cipherletter = letter
        ciphertext += cipherletter
    
    return ciphertext

def decrypt_caesar(ciphertext):
    """Decrypt a ciphertext using a Caesar cipher.
    """
    plaintext = ""
    for cipherletter in ciphertext:
        if ord(cipherletter.upper()) <= 90 and ord(cipherletter.upper()) >= 65:
            plainletter = chr((ord(cipherletter.upper()) - 68) % 26 + 65)
        else:
            plainletter = cipherletter
        plaintext += plainletter

    return plaintext

# Vigenere Cipher

def encrypt_vigenere(plaintext, keyword):
    """Encrypt plaintext using a Vigenere cipher with a keyword.
    """
    cipher_text = ''
    j = 0
    for letter in plaintext:
        if j == len(keyword):
            j = 0
        cipher_text += chr( (ord(letter.upper()) + ord(keyword[j].upper()) - 130 ) % 26 + 65)
        j+=1
    return cipher_text

def decrypt_vigenere(ciphertext, keyword):
    """Decrypt ciphertext using a Vigenere cipher with a keyword.
    """
    plaintext = ''
    j = 0
    for letter in ciphertext:
        if j == len(keyword):
            j = 0
        diff = ord(letter.upper()) - ord(keyword[j].upper())
        if diff >= 0:
            plaintext += chr(diff % 26 + 65)
        else:
            plaintext += chr(91 - abs(diff))
        j += 1
    return plaintext

#Scytale Cipher

def encrypt_scytale(plaintext, circum):
    """Encrypt plaintext using a Scytale cipher with a circum.
    """
    circum = int(circum)
    cipher_text = ''
    i = 0
    while len(cipher_text) != len(plaintext):
        j = i
        while j < len(plaintext):
            cipher_text += plaintext[j]

            j += circum
        i += 1

    return cipher_text


def decrypt_scytale(ciphertext, circum):
    """Decrypt ciphertext using a Scytale cipher with a circum.
    """
    plaintext = ''

    i = 0
    circum = round(len(ciphertext) / int(circum))

    while len(plaintext) != len(ciphertext):
        j = i
        while j < len(ciphertext):
            plaintext += ciphertext[j]
            j += circum
        
        i += 1

    return plaintext

#Railfence Cipher

def encrypt_railfence(plaintext, rail_num):
    """Encrypt plaintext using a Railfence cipher with a circum.
    """
    ciphertext = ''

    rail_num = int(rail_num)
    matrix = [['' for i in range(len(plaintext))] for j in range(rail_num)]

    row = 0
    check = 0
    for i in range(len(plaintext)):
        if check == 0:
            matrix[row][i] = plaintext[i]
            row += 1
            if row == rail_num:
                check = 1
                row -= 1
        elif check == 1:
            row -= 1
            matrix[row][i] = plaintext[i]
            if row == 0:
                check = 0
                row = 1
    ciphertext = ''
    for i in range(rail_num):
        for j in range(len(plaintext)):
            ciphertext += matrix[i][j]

    return ciphertext

def decrypt_railfence(ciphertext, rail_num):
    """Decrypt ciphertext using a Railfence cipher with a circum.
    """
    plaintext = ''
    rail_num = int(rail_num)
    matrix = [['' for i in range(len(ciphertext))] for j in range(rail_num)]

    row = 0
    check = 0
    for i in range(len(ciphertext)):
        if check == 0:
            matrix[row][i] = ciphertext[i]
            row += 1
            if row == rail_num:
                check = 1
                row -= 1
        elif check == 1:
            row -= 1
            matrix[row][i] = ciphertext[i]
            if row == 0:
                check = 0
                row = 1
    ordr = 0
    for i in range(rail_num):
        for j in range(len(ciphertext)):
            tmp = matrix[i][j]
            if tmp == '':
                continue
            else:
                matrix[i][j] = ciphertext[ordr]
                ordr += 1
    check = 0
    row = 0
    for i in range(len(ciphertext)):
        if check == 0:
            plaintext += matrix[row][i]
            row += 1
            if row == rail_num:
                check = 1
                row -= 1
        elif check == 1:
            row -= 1
            plaintext += matrix[row][i]
            if row == 0:
                check = 0
                row = 1
    
    return plaintext

# Merkle-Hellman Knapsack Cryptosystem

def generate_private_key(n=8):
    """Generate a private key for use in the Merkle-Hellman Knapsack Cryptosystem.

    Following the instructions in the handout, construct the private key components
    of the MH Cryptosystem. This consistutes 3 tasks:

    1. Build a superincreasing sequence `w` of length n
        (Note: you can check if a sequence is superincreasing with `utils.is_superincreasing(seq)`)
    2. Choose some integer `q` greater than the sum of all elements in `w`
    3. Discover an integer `r` between 2 and q that is coprime to `q` (you can use utils.coprime)

    You'll need to use the random module for this function, which has been imported already

    Somehow, you'll have to return all of these values out of this function! Can we do that in Python?!

    @param n bitsize of message to send (default 8)
    @type n int

    @return 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.
    """
    raise NotImplementedError  # Your implementation here

def create_public_key(private_key):
    """Create a public key corresponding to the given private key.

    To accomplish this, you only need to build and return `beta` as described in the handout.

        beta = (b_1, b_2, ..., b_n) where b_i = r × w_i mod q

    Hint: this can be written in one line using a list comprehension

    @param private_key The private key
    @type private_key 3-tuple `(w, q, r)`, with `w` a n-tuple, and q and r ints.

    @return n-tuple public key
    """
    raise NotImplementedError  # Your implementation here


def encrypt_mh(message, public_key):
    """Encrypt an outgoing message using a public key.

    1. Separate the message into chunks the size of the public key (in our case, fixed at 8)
    2. For each byte, determine the 8 bits (the `a_i`s) using `utils.byte_to_bits`
    3. Encrypt the 8 message bits by computing
         c = sum of a_i * b_i for i = 1 to n
    4. Return a list of the encrypted ciphertexts for each chunk in the message

    Hint: think about using `zip` at some point

    @param message The message to be encrypted
    @type message bytes
    @param public_key The public key of the desired recipient
    @type public_key n-tuple of ints

    @return list of ints representing encrypted bytes
    """
    raise NotImplementedError  # Your implementation here

def decrypt_mh(message, private_key):
    """Decrypt an incoming message using a private key

    1. Extract w, q, and r from the private key
    2. Compute s, the modular inverse of r mod q, using the
        Extended Euclidean algorithm (implemented at `utils.modinv(r, q)`)
    3. For each byte-sized chunk, compute
         c' = cs (mod q)
    4. Solve the superincreasing subset sum using c' and w to recover the original byte
    5. Reconsitite the encrypted bytes to get the original message back

    @param message Encrypted message chunks
    @type message list of ints
    @param private_key The private key of the recipient
    @type private_key 3-tuple of w, q, and r

    @return bytearray or str of decrypted characters
    """
    raise NotImplementedError  # Your implementation here
