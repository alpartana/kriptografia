from random import shuffle

def solitare(key, length):
    shuffle(key)

    deck = key

    key = []

    for _ in range(length):
        byte = 0
        step = 0
        while step < 2:
            index_1 = deck.index("A")
            index_2 = deck.index("B")
            if index_1 > index_2:
                index_1, index_2 = index_2, index_1
            
            new_index = (index_1 + 1) % 28
            if new_index == 0:
                new_index += 1
            deck[index_1], deck[new_index] = deck[new_index], deck[index_1]

            new_index = (index_2 + 1) % 28
            if new_index == 0 or new_index == 1:
                new_index += 1
            deck[index_2], deck[new_index] = deck[new_index], deck[index_2]

            deck = [*deck[index_2:], *deck[index_1 - 1:index_2], *deck[:index_1 - 1]]

            value = deck[-1]
            if value != "A" and value != "B":
                value = int(value)
                deck = [*deck[value:-1], *deck[:value], deck[-1]]
            
            if deck[0] != "A" and deck[0] != "B":
                step += 1
                x = deck[int(deck[0])]
                if x == "A":
                    x = 26
                elif x == "B":
                    x = 27
                else:
                    x = int(x)
                byte += x % 16
                if step == 1:
                    byte = byte << 4
        key.append(chr(byte))
    return "".join(key)
