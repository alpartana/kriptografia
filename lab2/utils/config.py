def getConfig(int):
    if int == 1:
        return {
            "algorithm": "blum_blum_shub",
            "key": 1249142
        }
    return {
        "algorithm": "solitare",
        "key": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,"A", "B"]
    }