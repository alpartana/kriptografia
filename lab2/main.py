from utils.solitare import solitare
from utils.blum_blum_shub import blum_blum_shub
from utils.config import getConfig

data = getConfig(1) 

def sxor(s1,s2):    
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

def encoder(text, method, key):
    if method == "solitare":
        method = solitare
    else:
        method = blum_blum_shub
    byts = method(key, len(text))
    ciphered_text = sxor(text, byts)
    return ciphered_text

ciphered = encoder(input("Input"), data['algorithm'], data['key'])
print(ciphered)
plain = encoder(ciphered, data['algorithm'], data['key'])
print(plain)